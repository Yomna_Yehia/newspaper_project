from django.urls import path
from .views import ArticleAPIView , DetailArticle , ListUser , DetailUser
# , PagesAPIView , UsersAPIView

urlpatterns =[
    path("" , ArticleAPIView.as_view()),
    path("<int:pk>" ,DetailArticle.as_view()),
    path("users/" , ListUser.as_view()),
    path("users/<int:pk>/" , DetailUser.as_view())
]